package com.example.list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.actionbarsherlock.app.SherlockActivity;

public class MainActivity extends SherlockActivity {

	public static final String EXTRA_FIRST_NAME = "first_name";
	public static final String EXTRA_LAST_NAME = "last_name";
	public static final String EXTRA_DOB = "dob";

	private BaseAdapter adapter;

	private Spinner spinner;

	private ArrayList<Student> students;

	@SuppressWarnings("unchecked")
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState != null && savedInstanceState.containsKey("list")) {
			students = (ArrayList<Student>) savedInstanceState.getSerializable("list");
		} else {
			students = Generator.generate();
		}
		
		adapter = new StudentAdapter(students, getApplicationContext());
		
		OnItemClickListener listener = new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Intent intent = new Intent(MainActivity.this,
						DetailActivity.class);

				Student student = (Student) adapter.getItem(position);

				intent.putExtra(EXTRA_FIRST_NAME, student.firstName);
				intent.putExtra(EXTRA_LAST_NAME, student.lastName);
				intent.putExtra(EXTRA_DOB, student.dob);

				startActivity(intent);
			}
		};

		View list = findViewById(R.id.list);
		
		if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
			
			if (list instanceof ListView){
				ListView l1 = (ListView) list; 
				l1.setAdapter(adapter);
				l1.setOnItemClickListener(listener);
			}else if (list instanceof GridView){
				
				GridView g1 = (GridView) list;
				g1.setAdapter(adapter);
				g1.setOnItemClickListener(listener);
			}

		} else {

			AbsListView al1 = (AbsListView)list;
			al1.setAdapter(adapter);
			al1.setOnItemClickListener(listener);
		}

		String[] titles = getResources().getStringArray(R.array.titles);

		spinner = (Spinner) findViewById(R.id.sort);

		SpinnerAdapter spinnerAdapter = new ArrayAdapter<String>(
				getApplicationContext(), android.R.layout.simple_list_item_1,
				titles);

		spinner.setAdapter(spinnerAdapter);

		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				switch (position) {
				case 0:
					sortListByName();
					break;
				case 1:
					sortListByAge();
					break;
				default:
					break;
				}
				adapter.notifyDataSetChanged();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

	}

	private void sortListByAge() {
		Collections.sort(students, new AgeComparator());
	}

	private void sortListByName() {
		Collections.sort(students, new NameComparator());
	}


	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		getSupportMenuInflater().inflate(R.menu.main, menu);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		return true;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("list", students);
	}

	private class NameComparator implements Comparator<Student> {
		@Override
		public int compare(Student o1, Student o2) {
			if (!o1.firstName.equals(o2.firstName)) {
				return o1.firstName.compareTo(o2.firstName);
			} else {
				return o1.lastName.compareTo(o2.lastName);
			}
		}
	}

	private class AgeComparator implements Comparator<Student> {
		@Override
		public int compare(Student o1, Student o2) {
			return o1.dob - o2.dob;
		}
	}
}
