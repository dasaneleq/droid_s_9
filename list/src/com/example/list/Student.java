package com.example.list;

import java.io.Serializable;

public class Student implements Serializable{

	private static final long serialVersionUID = 1L;

	public final String firstName;
	public final String lastName;
	public final int dob;

	public Student(String firstName, String lastName, int dob) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.dob = dob;
	}
	
	@Override
	public String toString() {
		return firstName + " " + lastName;
	}
	
}
