package com.example.list;

import java.util.ArrayList;
import java.util.Random;

public class Generator {
	
	public static String[] firstNames = {"John", "Jeff", "Bill", "Jordan", "Nick", "Dara", 
		"Chantelle", "Elda", "Dan", "Minerva", "Margeret", "Michale", "Karly", "Vivian", 
		"Amelia", "Pasquale", "Tiara", "Kimi", "Milda", "Tami", "Denae", "Teressa", "Roma", 
		"Romeo", "Terra", "Lue", "Lucius", "Stevie", "Launa", "Cyrus", "Jamal", "Yessenia", 
		"Nadine", "Jonas", "Ethan", "Cinderella", "Marquetta", "Kendrick", "Margarete", 
		"Christy", "Imogene", "Shamika", "Armanda", "Trey", "Darleen", "Beatris", "Lesley", 
		"Teena", "Refugia", "Emilia", "Sun", "Hermila", "Pearlene", "Mozella", "Shemeka"};
	public static String[] lastNames = {"Smith", "Bond", "Gates", "Trump", "Gogol", "Spradling", 
		"Brissette", "Marsala", "Weissman", "Bilski", "Mcconnel", "Larkey", "Fouche", "Lamantia", 
		"Gaccione", "Lipsky", "Spagnoli", "Koller", "Ortman", "Rowan", "Attaway", "Shutts", 
		"Steckel", "Severa", "Hersey", "Gladding", "Mcguffie", "Lozano", "Bullock", "Masi", 
		"Sankey", "Allinder", "Easter", "Basil", "Norberg", "Slocum", "Whiteley", "Arthurs", 
		"Tinsley", "Schwab", "Nawrocki", "Chenard", "Stogner", "Bopp", "Colburn", "Gadbois", 
		"Riffe", "Jeffers", "Plumber", "Rathbun", "Trumbo", "Valls", "Huber", "Schuelke", "Capps"};
	
	public static ArrayList<Student> generate(){
		ArrayList<Student> students = new ArrayList<Student>();
		
		Random rand = new Random();
		
		for (int i = 0; i<100; i++){
			Student student = new Student(firstNames[rand.nextInt(firstNames.length)], 
					lastNames[rand.nextInt(lastNames.length)], 
					1913 + rand.nextInt(100));
			students.add(student);
		}
		
		return students;
	}
	

}
